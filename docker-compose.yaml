version: '3.7'

services:
  app: &app
    build:
      context: .
      dockerfile: .gitlab/python/local.Dockerfile
    container_name: blog_app
    command: make production
    restart: always
    env_file: .env
    depends_on:
      database:
        condition: service_healthy
    volumes:
      - "${PWD}:/app"
      - gunicorn:/run/gunicorn
    healthcheck:
      test: ["CMD", "curl", "-f", "--unix-socket", "/run/gunicorn/socket", "http:/localhost/posts"]
      interval: 5m
      timeout: 10s
      retries: 3
      start_period: 5s
    networks:
      - fauna

  database:
    image: postgres:13.1
    container_name: blog_postgres
    restart: always
    environment:
      POSTGRES_DB: blog
      POSTGRES_USER: blog
      POSTGRES_PASSWORD: blog
      PG_VERSION: 13
      PGDATA: /var/lib/postgresql/data
    volumes:
      - ./database:/var/lib/postgresql/data
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U blog"]
      interval: 10s
      timeout: 5s
      retries: 5
    networks:
      - fauna

  nginx:
    build:
      context: .
      dockerfile: .gitlab/nginx/local.Dockerfile
    container_name: blog_proxy
    restart: always
    depends_on:
      - app
    ports:
      - "80:80"
    volumes:
      - ./static/:/static
      - ./media/:/media
      - gunicorn:/run/gunicorn
    healthcheck:
      test: ["CMD", "service", "nginx", "status"]
      interval: 10s
      timeout: 5s
      retries: 5
    networks:
      - fauna

  migrate:
    <<: *app
    container_name: blog_migrate
    command: make migrate
    ports: []
    restart: "no"

volumes:
  gunicorn:

networks:
  ### You should create that network externally
  ### example: `docker network create -d bridge fauna`
  fauna:
    external: true