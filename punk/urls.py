from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views
from blog.forms import LoginForm


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('blog.urls')),
    path('login', views.LoginView.as_view(authentication_form=LoginForm), name='login', kwargs={'next': '/posts'}),
    path('logout', views.LogoutView.as_view(), name='logout', kwargs={'next': '/posts'}),
]
