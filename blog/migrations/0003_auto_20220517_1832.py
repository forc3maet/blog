# Generated by Django 3.0.4 on 2022-05-17 18:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20200419_0401'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comment',
            name='header',
        ),
        migrations.RemoveField(
            model_name='post',
            name='author',
        ),
    ]
