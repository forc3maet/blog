from django.urls import path
from blog import views


urlpatterns = [
    path('', views.AboutView.as_view(), name='about'),
    path('drafts', views.DraftListView.as_view(), name='draft_list'),
    path('posts', views.PostListView.as_view(), name='post_list'),
    path('post/add', views.PostCreateView.as_view(), name='post_add'),
    path('post/<int:pk>', views.PostOverView.as_view(), name='post_content'),  # TODO: add post title as slug url
    path('post/<int:pk>/edit', views.PostUpdateView.as_view(), name='post_edit'),
    path('post/<int:pk>/remove', views.PostDeleteView.as_view(), name='post_remove'),
    path('post/<int:pk>/publish', views.publish_post, name='post_publish'),
    path('post/<int:pk>/comment', views.add_comment_to_post, name='comment_post'),
    path('comment/<int:pk>/approve', views.approve_comment, name='comment_approve'),
    path('comment/<int:pk>/remove', views.delete_comment, name='comment_remove'),
]
