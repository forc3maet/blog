from django import template
from django.template.defaultfilters import stringfilter
import mistune


register = template.Library()

@register.filter
@stringfilter
def markdown(value):
    markdown = mistune.create_markdown(plugins=['speedup', 'strikethrough', 'mark', 'insert', 'superscript', 'subscript', 'footnotes', 'table', 'url', 'abbr', 'def_list', 'math', 'ruby', 'task_lists', 'spoiler'])
    return markdown(value)
