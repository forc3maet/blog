from django.db import models
from django.utils import timezone
from django.urls import reverse


class Post(models.Model):
    title = models.CharField(max_length=256)
    content = models.TextField()
    create_date = models.DateTimeField(default=timezone.now)
    publish_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.publish_date = timezone.now()
        self.save()

    def approve_comment(self):
        return self.comment.filter(approved=True)

    def get_absolute_url(self):
        return reverse("post_content", kwargs={'pk': self.pk})

    def __str__(self):
        return self.title


class Comment(models.Model):
    author = models.CharField(max_length=200)
    post = models.ForeignKey('blog.Post', related_name='comment', on_delete=models.CASCADE)
    content = models.TextField()
    create_date = models.DateTimeField(default=timezone.now)
    approved = models.BooleanField(default=False)

    def __str__(self):
        return self.header

    def get_absolute_url(self):
        return reverse("post_list")

    def approve(self):
        self.approved = True
        self.save()
