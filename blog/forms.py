from django import forms
from blog.models import Post, Comment
from django.contrib.auth.forms import AuthenticationForm, UsernameField


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'content')

        widgets = {
            'title': forms.TextInput(attrs={'class': 'post_title',
                                            'size': 50}),
            'content': forms.Textarea(attrs={'class': 'post_content editable',
                                             'oninput': 'this.editor.update()',
                                             'id': 'content'}),
        }
        labels = {
            'title': '',
            'content': ''
        }


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('author', 'content')

        widgets = {
            'author': forms.TextInput(attrs={'class': 'comment_title', 'value': 'Anon'}),
            'content': forms.Textarea(attrs={'class': 'comment_content editable'}),
        }


class LoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

    username = UsernameField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Login',
            'id': 'usernameInput'
        }
    ))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Password',
            'id': 'passwordInput'
        }
    ))

