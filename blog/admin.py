from django.contrib import admin
from blog.models import Post, Comment


# Register your models here.
class PostAdmin(admin.ModelAdmin):
    list_display = ["title", "content"]

class CommentAdmin(admin.ModelAdmin):
    list_display = ["author", "content", "post"]

admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)
