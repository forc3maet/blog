from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.utils import timezone
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView, DetailView, ListView, CreateView, UpdateView  #, DeleteView
from django.views.generic.edit import DeleteView
from blog.models import Post, Comment
from blog.forms import PostForm, CommentForm


class AboutView(TemplateView):
    template_name = 'blog/about.html'


class PostListView(ListView):
    model = Post

    def get_queryset(self):
        return Post.objects.filter(publish_date__lte=timezone.now()).order_by('publish_date')


class PostOverView(DetailView):
    model = Post


class PostCreateView(LoginRequiredMixin, CreateView):
    login_url = 'login'
    redirect_field_name = 'post/<int:pk>'

    model = Post
    form_class = PostForm


class PostUpdateView(LoginRequiredMixin, UpdateView):
    login_url = 'login'
    redirect_field_name = 'post/<int:pk>'

    model = Post
    form_class = PostForm


class PostDeleteView(LoginRequiredMixin, DeleteView):
    login_url = 'login'
    success_url = reverse_lazy('draft_list')
    redirect_field_name = 'posts'

    model = Post
    form_class = PostForm


class DraftListView(LoginRequiredMixin, ListView):
    login_url = 'login'
    redirect_field_name = 'posts'
    template_name = 'blog/post_draft_list.html'

    model = Post

    def get_queryset(self):
        return Post.objects.filter(publish_date__isnull=True).order_by('create_date')


@login_required
def add_comment_to_post(request, pk):
    post = get_object_or_404(Post, pk=pk)

    if request.method == 'POST':
        form = CommentForm(request.POST)

        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.save()
            return redirect('post_content', pk=post.pk)
    else:
        form = CommentForm()
    return render(request, 'blog/comment_form.html', {"form": form})


@login_required
def approve_comment(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    comment.approve()
    return redirect('post_content', pk=comment.post.pk)


@login_required
def delete_comment(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    post_pk = comment.post.pk
    comment.delete()
    return redirect('post_content', pk=post_pk)


@login_required
def publish_post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.publish()
    return redirect('post_content', pk=pk)
