# See:
# http://www.gnu.org/software/make/manual/make.html
# http://linuxlib.ru/prog/make_379_manual.html

# Set shell interpreter
SHELL := /bin/bash

migrate: ## Apply database schema changes
	python manage.py makemigrations
	python manage.py migrate --noinput

admin:  ## Create superuser for application
	python manage.py createsuperuser

static:  ## Generate modules staticfiles
	python manage.py collectstatic --no-input

prepare:  ## Setup application static and database
	python manage.py makemigrations
	python manage.py migrate --noinput
	python manage.py collectstatic --no-input

dev:  ## Run dev server
	python manage.py runserver 0.0.0.0:8000

production:  ## Run production setup
	gunicorn --config=gunicorn_config.py --log-config gunicorn_logging.conf punk.wsgi
	#gunicorn --config=gunicorn_config.py --log-config gunicorn_logging.conf punk.asgi

