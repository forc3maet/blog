FROM nginx:1.17.8-alpine
CMD ["nginx", "-g", "daemon off;"]
RUN apk add --no-cache bash

COPY .gitlab/nginx/nginx.conf /etc/nginx/nginx.conf
COPY .gitlab/nginx/default.conf /etc/nginx/conf.d/default.conf