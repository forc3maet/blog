FROM python:3.9-slim-buster

WORKDIR /app
ENV PYTHONPATH=/app:$PYTHONPATH

RUN apt-get update && \
    apt-get install -y libpq-dev gcc postgresql postgresql-contrib nano bash curl make

COPY reqs.txt reqs.txt

RUN pip install --upgrade pip && \
    pip install --no-cache-dir -r reqs.txt
